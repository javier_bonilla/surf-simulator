---
layout: project
title:  "EUROTROUGH PARABOLIC TROUGH COLLECTOR SIMULATOR"
date:   2017-10-16 12:00:00
author: Javier Bonilla, Adriano Desideri, Rémi Dickes, Loreto Valenzuela
contact: Javier Bonilla
email: javier.bonilla@psa.es
categories:
- project
img: ET.png
thumb: thumb01.jpeg
carousel:
- ET1.png
- ET2.png
- ET3.png
- ET4.png
tagged: Solar, Parabolic Trough Collector (PTC), Thermal Energy
website: https://gitlab.com/ciemat-psa/surf/tree/master/examples/EuroTrough
releases:
    - version: 0.1
      date:  2017-10-16 12:00:00
      description: First version.
      winapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/win32/EuroTrough/v0.1/EuroTrough.exe
      linuxapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/linux64/EuroTrough/v0.1/EuroTrough-x86_64.AppImage
---
#### EuroTrough Parabolic Trough Collector (PTC) Simulator
<div class="hline"></div>
This application simulates a EuroTrough parabolic trough collector (PTC). Simulation results are compared against experimental data from the [HTF test loop](http://www.psa.es/en/instalaciones/parabolicos/htf.php) at [CIEMAT](http://www.ciemat.es) - [Plataforma Solar de Almería (PSA)](http://www.psa.es/en/). An experimental campaign was perform in the scope of the [Solar Facilities for the European Research Area (SFERA) II project](http://sfera.sollab.eu/) in order to calibrate and validate the PTC dynamic model. The dynamic PCT model is implemented in [Modelica](https://www.modelica.org/) and is included in the open source [ThermoCycle](http://www.thermocycle.net/) library.

#### The HTF Test Loop
<div class="hline"></div>
The HTF test loop was erected in 1997 and it is an ideal facility for evaluating parabolic trough collector components under real solar energy operating conditions.

The facility consists of a closed thermal-oil circuit connected to several solar collectors of 75-m long connected in parallel (up to three collectors can be installed in parallel), being able to operate only one at a time. The east-west rotating axis of the solar collectors increases the number of hours per year in which the angle of incidence of the solar radiation is less than 5º. The thermal oil used in this facility (Syltherm 800) has a maximum working temperature of 420ºC and a freezing point of -40ºC.

The facility’s oil circuit, which has a maximum working pressure of 20 bar, is made up of the following elements:

* 1-m<sup>3</sup>-capacity oil expansion tank, with automatic nitrogen inertization
* Oil circuit sump tank.
* Mechanical-draft oil cooler, with air speed control and 400-kW maximum cooling
* Centrifugal oil pump, with a flow rate of up to 8.3 liters per second
* Two 40-kW electric oil heaters

![HTF](http://www.psa.es/en/instalaciones/images/grafico_htf.jpg)

The first EuroTrough collector prototype developed by an European consortium with the financial aid of the European Commission was installed and evaluated under real working conditions at this facility in 1998 The facility is appropriately instrumented for qualifying and monitoring of the following components:

* New designs of parabolic trough collectors (up to 75 m long)
* Parabolic trough collector mirrors
* Parabolic trough collector absorber tubes
* New designs of ball-joints or flexholes for connecting parabolic trough collectors in the solar fields.
* Solar tracking systems.

Main activities are currently related to study the optical and thermal performance of complete parabolic trough collectors (optical efficiency, IAM coefficient, and global efficiency/heat losses) and receiver tubes.
