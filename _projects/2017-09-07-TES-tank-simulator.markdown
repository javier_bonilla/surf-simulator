---
layout: project
title:  "THERMAL ENERGY STORAGE (TES) TANK SIMULATOR"
date:   2017-09-06 12:00:00
author: Javier Bonilla
contact: Javier Bonilla
email: javier.bonilla@psa.es
categories:
- project
img: TES_main.png
thumb: thumb01.jpeg
carousel:
- TES1.png
- TES2.png
- TES3.png
- TES4.png
tagged: Solar, Thermal Energy Storage, Molten salt
website: https://gitlab.com/ciemat-psa/surf/tree/master/examples/Tank
releases:
    - version: 0.1
      date:  2017-09-06 12:00:00
      description: First version.
      winapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/win32/TankSim/v0.1/TankSim.exe
      linuxapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/linux64/TankSim/v0.1/TankSim-x86_64.AppImage
    - version: 0.2
      date:  2017-10-22 12:00:00
      description: Minor improvements and bug fixes.
      winapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/win32/TankSim/v0.2/TankSim.exe
      linuxapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/linux64/TankSim/v0.2/TankSim-x86_64.AppImage      
---
#### Thermal Energy Storage (TES) Tank Simulator
<div class="hline"></div>
This application simulate a TES tank, where the storage fluid is molten salt. Three different cases are considered: charging process, discharging process and thermal losses at rest. Simulation results are compared against experimental data from a real tank in the 
[Molten Salt Test Loop for Thermal Energy Systems (MOSA)](http://www.psa.es/en/instalaciones/almacenamiento.php) facility at [CIEMAT](http://www.ciemat.es) - [Plataforma Solar de Almería (PSA)](http://www.psa.es/en/).

#### Molten Salt Test Loop for Thermal Energy Systems (MOSA)
<div class="hline"></div>
This molten salt test loop is a replica of a thermal energy storage system with molten salts and a two-tank configuration. 

![MOSA](http://www.psa.es/en/instalaciones/images/almacenamiento1.jpg)

This facility consists basically in:

* Two tanks, one vertical, for hot molten salts, and another horizontal, for cold molten salts.
* A thermal oil loop that can be used for heating the salt up to 380ºC and cooling it to 290ºC.
* A CO<sub>2</sub>-molten salt heat exchanger for heating the salt up to 500ºC with CO<sub>2</sub> supplied by parabolic trough collectors.
* Two flanged sections, where different components for this type of loops (e.g. valves, flow meters, heat trace, pumps…) can be tested.

Being a set up which is a reduced scale of a commercial two-tank molten salt storage system, everything related to this type of systems can be tested in this facility.
