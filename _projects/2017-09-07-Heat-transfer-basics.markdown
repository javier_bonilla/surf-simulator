---
layout: project
title:  "HEAT TRANSFER BASICS SIMULATOR"
date:   2017-09-06 12:00:00
author: Javier Bonilla
contact: Javier Bonilla
email: javier.bonilla@psa.es
categories:
- project
img: HTB_main.png
thumb: thumb01.jpeg
carousel:
- HTB1.png
- HTB2.png
- HTB3.png
- HTB4.png
- HTB5.png
- HTB6.png
- HTB7.png
- HTB8.png
tagged: Heat transfer, education, convection, conduction, radiation
website: https://gitlab.com/ciemat-psa/surf/tree/master/examples/HeatTransferBasics
releases:
    - version: 0.1
      date:  2017-09-06 12:00:00
      description: First version.
      winapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/win32/HeatTransferBasics/v0.1/HeatTransferBasics.exe
      linuxapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/linux64/HeatTransferBasics/v0.1/HeatTransferBasics-x86_64.AppImage
---
#### Heat Transfer Basics Simulator
<div class="hline"></div>
This application includes fourteen basic heat transfer problems for educational purposes. Basic concepts about convective, conduction and radiation heat transfer are considered. The presented examples are extracted from the first chapter of the following book.

<p align="center">
<img src="http://highered.mheducation.com/sites/dl/free/0073398187/cover/Cengel5e15jl_nm2.jpg" alt="Book image">
</p>

**Heat and Mass Transfer: Fundamentals and Applications**, 5<sup>th</sup> edition<br>
**Yunus A. Cengel**, Adnan Menderes University<br>
**Afshin J. Ghajar**, Oklahoma State University<br>
ISBN: 0073398187<br>
Copyright year: 2015<br>
<a href="http://highered.mheducation.com/sites/0073398187/" target="_blank">Book link</a>
