---
layout: project
title:  "SOLARTHERMOPRO SIMULATOR"
date:   2017-09-06 12:00:00
author: Javier Bonilla
contact: Javier Bonilla
email: javier.bonilla@psa.es
categories:
- project
img: STP_main.png
thumb: thumb03.jpeg
carousel:
- STP1.png
- STP2.png
- STP3.png
tagged: Solar, Thermodynamic Properties, Molten salt, Thermal oil
website: https://gitlab.com/ciemat-psa/surf/tree/master/examples/SolarThermoPro
releases:
    - version: 0.1
      date:  2017-09-06 12:00:00
      description: First version.
      winapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/win32/SolarThermoPro/v0.1/SolarThermoPro.exe
      linuxapp: https://gitlab.com/ciemat-psa/surf/blob/master/bin/linux64/SolarThermoPro/v0.1/SolarThermoPro-x86_64.AppImage
---
#### SolarThermoPro Simulator
<div class="hline"></div>
This application provides thermodynamic properties values (density, dynamic viscosity, thermal conductivity, specific heat capacity, etc.) from thermodynamic properties models given by different authors of common fluid used in solar thermal applications (thermal oil, molten salt, etc.) within a selected temperature range. It is still at early development stage. The final goal is to provide a comprehensive database of thermodynamic properties models for different kinds of fluids.

Presently, the following fluids are considered in the application, although they will be completed with thermodynamic properties models from other sources.

* Molten salt
    * Solar salt
    * Hitec
    * Hitec XL
* Thermal oil
    * Therminol 55
    * Therminol 59
    * Therminol VP1
